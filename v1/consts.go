package v1

type ApiType uint8

const (
	Error     ApiType = iota
	AllCities ApiType = iota
	Alliance  ApiType = iota
)

const apiPoint = "https://politicsandwar.com/api/"
