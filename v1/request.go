package v1

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// request handles the getting and parsing of JSON from any API endpoint required.
func request(path string) (ApiStruct, error) {
	var (
		output = apiStructReceiver{}
		err    error

		response *http.Response
		body     []byte
	)

	url := apiPoint + path
	response, err = http.Get(url)
	if err != nil {
		goto RETURN
	}

	body, err = ioutil.ReadAll(response.Body)
	if err != nil {
		goto RETURN
	}

	err = json.Unmarshal(body, &output)
	if err != nil {
		goto RETURN
	}

RETURN:
	return newApiStruct(output), err
}
