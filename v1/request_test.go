package v1

import (
	"os"
	"testing"
)

// TestRequest tests request, an unexported function used in v1. Environment
// variable PNW should be holding your api key.
func TestRequest(t *testing.T) {
	key, ok := os.LookupEnv("PNW")
	if !ok {
		t.Fatal("Environment variable not set")
	}

	json, err := request("all-cities?key=" + key)
	if err != nil {
		t.Fatal(err.Error())
	}
	if !json.Success {
		t.Fatal(json.GeneralMessage)
	}
}
