package v1

// apiStructReceiver directly receives the JSON data.
type apiStructReceiver struct {
	ApiType

	Success        bool   `json:"success"`
	GeneralMessage string `json:"general_message"`

	AllCities []citiesReceiver `json:"all_cities"`
}

// citiesReceiver directly receives the JSON data for /all-cities.
type citiesReceiver struct {
	NationID string `json:"nation_id"`
	CityID   string `json:"city_id"`
	CityName string `json:"city_name"`
	Capital  string `json:"capital"`
	Infra    string `json:"infrastructure"`
	MaxInfra string `json:"maxinfra"`
	Land     string `json:"land"`
}
