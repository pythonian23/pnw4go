package v1

import "strconv"

// newApiStruct constructs an ApiStruct from an apiStructReceiver, while also
// converting the necessary values to their proper types.
func newApiStruct(receiver apiStructReceiver) ApiStruct {
	return ApiStruct{
		apiStructReceiver: receiver,
		AllCities:         newCitiesSlice(receiver.AllCities),
	}
}

// ApiStruct is the struct type that stores the result of every api result.
type ApiStruct struct {
	apiStructReceiver

	AllCities []Cities
}

// newCitiesSlice converts a slice of citiesReceiver to a slice of Cities.
func newCitiesSlice(receiver []citiesReceiver) []Cities {
	cities := make([]Cities, len(receiver), len(receiver))
	for i, r := range receiver {
		var (
			nationID, _ = strconv.Atoi(r.NationID)
			cityID, _   = strconv.Atoi(r.CityID)
			capital     = r.Capital == "1"
			infra, _    = strconv.ParseFloat(r.Infra, 64)
			maxInfra, _ = strconv.ParseFloat(r.MaxInfra, 64)
			Land, _     = strconv.ParseFloat(r.Land, 64)
		)
		cities[i] = Cities{
			NationID: nationID,
			CityID:   cityID,
			CityName: r.CityName,
			Capital:  capital,
			Infra:    infra,
			MaxInfra: maxInfra,
			Land:     Land,
		}
	}
	return cities
}

// Cities is part of the result of the /all-cities API.
type Cities struct {
	NationID int
	CityID   int
	CityName string
	Capital  bool
	Infra    float64
	MaxInfra float64
	Land     float64
}
