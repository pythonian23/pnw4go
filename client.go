package pnw4go

// NewClient constructs a new Client.
func NewClient(key string) Client {
	return Client{
		client{
			key: key,
		},
	}
}

// Client is the base for all requests.
type Client struct {
	client
}

// TODO: CachedClient

type client struct {
	key string
}
